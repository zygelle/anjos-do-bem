class ChangeAlbumAttribs < ActiveRecord::Migration[5.2]
  def change
    remove_column :albums, :album_photo_id
    add_column :albums, :cover_photo, :string
  end
end
