class AlbumPhotosController < ApplicationController
  before_action :authenticate_admin!
  before_action :set_album_photo, only: [:destroy]

  def create
    @album_photo = AlbumPhoto.new(album_photo_params)

    respond_to do |format|
      if @album_photo.save
        format.json {render json: @album_photo, status: :ok}
      else
        format.json {render json: @album_photo.errors.full_messages, status: :unprocessable_entity}
      end
    end
  end
  
  def destroy
    @album_photo.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private
    def album_photo_params
      params.require(:album_photo).permit(:album_id, :content)
    end

    def set_album_photo
      @album_photo = AlbumPhoto.find(params[:id])
    end
end
