json.extract! success_story, :id, :pet_name, :owner, :photo, :description, :created_at, :updated_at
json.url success_story_url(success_story, format: :json)
