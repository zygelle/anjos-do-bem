class Animal < ApplicationRecord
  has_many :animal_photos, dependent: :destroy
  
  enum sex: {female: false, male: true}
end
