class AnimalPhoto < ApplicationRecord
  belongs_to :animal
  validates :content, presence: true
  mount_uploader :content, PhotoUploader
end
