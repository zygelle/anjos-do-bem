Rails.application.routes.draw do

  root to: "pages#home"
  devise_for :admins, path: '', path_names: { sign_in: 'login', sign_out: 'logout', password: 'segredo', confirmation: 'verificacao', unlock: 'desbloqueio', registration: 'registro', sign_up: 'entrar' }
  
  scope(path_names: { new: 'criar', edit: 'editar' }) do
    resources :albums
    post 'fotos-album/', to: 'album_photos#create', as: 'album_photos'
    delete 'fotos-album/:id', to: 'album_photos#destroy', as: 'album_photo'
    resources :teammates, path: 'membros-da-equipe'
    resources :success_stories, path: 'historias-de-sucesso'
    resources :animals, path: 'bichos'
    post 'fotos-animal', to: 'animal_photos#create', as: 'animal_photos'
    delete 'fotos-animal/:id', to: 'animal_photos#destroy', as: 'animal_photo'

  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
